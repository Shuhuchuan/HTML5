﻿笔记
=============================================================
文强 		
tel:13718079028
QQ:1677160576
qq群：138876501

论坛：bbs.zhinengshe.com

缺点：
语速过快


=================================================
上课时间
9:30-12:30
13:30-4:30
===================================================
js
onload 			所有的加载完再执行
DOMReady 		dom元素加载完毕执行
DOMReady事件
	DOMContentLoaded 		
	onreadystatechange 		

获取元素
document.getElementById
obj.getElementsByClassName
obj.getElementsByTagName
document.getElementsByName
父级.children
父级.childNodes


DOM操作
父级 
obj.parentNode 			结构上的 		document
obj.offsetParent 		定位上的 		body
子级
父级.children 			只能获取第一层
父级.childNodes 		文本节点和标签节点都获取
		obj.nodeType 		查看节点类型

事件
事件绑定：给同一个元素加同一个事件加多次。
obj.addEventListener 		高级浏览器
obj.attachEvent 			低版本ie

事件流
	DOM事件流 	冒泡 	捕获
	IE事件流 	冒泡

事件冒泡取消
oEvent.cancelBubble = true;

return false; 	阻止默认事件

遇到addEventListener    return false不好使
oEvent.preventDefault();

=================================================
html5/css3
1.什么是html5,css3？
 		html4 	标签，属性
 		css2 	样式

 		html5 	新标签，新属性
 		css3 	新样式

h5开发		80%用的还是之前的东西。

2.h5,css3 		能干啥？
	动画
	游戏
	图表

	定位

	移动端：手机、pad、watch、电视

		移动端的做法跟pc端一样。只是尺寸有区别
		移动端比pc端简单。内容少。


	App:
		原生app：
			oc,java
			oc 			ios
			java 		安卓
			优势：性能高，调用手机底层的东西
			缺点：开发成本高，更新麻烦，开发周期比较长
		webapp：
			就是你平常写的网页，只是套了个壳子(phoneGap后面说)
			优点：开发成本低，开发效率快，更新方便
			缺点：性能不高，调用不了底层的东西。
		混合app：
			webapp+原生app
			要的只是一些底层功能。

html5
	新增标签
		header,footer,banner,nav,section.........
		canvas,video......

		"没用的"
			header,footer,banner,nav,section
			作用：提高语义化
			建议：公司其他人用，你就用，其他人不用，不用。
		有用的
			新的标签有功能，其他人替代不了
			canvas,video.....
			画布，画图
			视频
			音频

	新的属性
		自定义属性
		data-自定义属性
			<input type="text" data-index="a" />
			oTxt.dataset.index = 'a';

			1.向后兼容 	html5
				w3c说：data-自定义属性	

			2.便于遍历

			3.dataset 	性能差


		移动端不能用jquery
		选择器：
		document.querySelectorAll('');
			获取一组元素
		document.querySelector('');
			获取一个元素	如果有多个，默认获取第一个



好消息：只要玩了h5，再也不能担心兼容了。
	html5不兼容
	chrome 		完全兼容
	firefox 	完全兼容
	ie 			9+		ie9:30%



以后别玩window.onload了，用DOMReady

obj.classList 		里面存储class
添加class
obj.classList.add('className');
删除class
obj.classList.remove('className');
查看这个元素中是否包含某个class
obj.classList.contains('className');
	有   true 		没有   false
切换
obj.classList.toggle('className');
	如果有就干掉，否则就加上

本地存储
	在服务器环境下运行
	cookie
	localStorage
	添加
	localStorage.name = value;
	获取
	localStorage.name;
	删除属性
	delete 属性


	对比
	cookie
		用法：复杂
		容量：4KB
		传输：随着交互而传输到服务器
		安全性：安全性差
		周期：有生存周期
	localStorage
		用法：简单
		容量：5MB
		传输：永远在本地，不会提交到服务器
		安全性：安全性好
		周期：没有

		ie7+

		localStorage 非常重要。


	delete 		删除属性

2.文档头
	<!doctype html> 		html5的文档声明,严格模式

	<img src="" />
	<br />
	单标签结束不用加/.
	<img src="">
	<br>
	建议加上

3.form表单
	默认的样式

	自己提供了表单验证 		没多大用处

4.获取地理信息
	地图
	周边
	附近
	大数据统计

	硬件支持
	pc 		ip地址
	移动 	gps

5.绘图功能
	canvas 		动画，游戏

	画矢量图，图表，图标，地图
	svg	 	不兼容低版本ie
	vml 	低版本ie用vml

6.WebSocket 		
	ajax 		单向
 			浏览器	->	服务器
	WebSocket 	双向
			浏览器  <->	服务器

	性能:WebSocket高

7.WebWorker

	传统的js是单线程的

	WebWorker可以开多线程

	线程 		同时做几件事

	使用webworker性能会有很大的提高

8.WEBGL
	之前的都是2D
	使用WEBGL能实现3D

	做游戏就用到了WEBGL

9.WEBSQL 		离线存储
	给前端用的数据库

	没用



h5
画图
表单
位置
存储
交互
线程
3D

========================================================
CSS3
css3跟css2完全一样。都是修改样式的
css3比css2多加了一些样式。

css3性能要比css2好。

改变一个div的位置,left\top

css2 	改变的是盒子模型
		重绘、重排
css3 	不改变盒子模型
改变的只是你的视觉。		操作的是GPU



css3
样式前缀
-webkit-width 		chrom,safari 	opera
-moz-width 			firefox
-o-width 			opera	也开始使用-webkit-的内核
-ms-width 			ie
width

必须要加的
-webkit-
-moz-
-ms-
不加前缀

js写css 		遇到-要大写

-webkit-transform 				WebkitTransform
-moz-transform 					MozTransform
-o-transform 					OTransform
-ms-transform 					msTransform





圆角
border-radius		px 		%	
border-radius:一个值;		四个角一起设置
border-radius:左上右下 	右上左下;
border-radius:左上  右上左下   右下;
border-radius:左上  右上  右下  左下;


阴影
	box-shadow:2px 2px 5px #000;

	box-shadow:x偏移 y偏移 阴影宽度 颜色;


渐变
	线性渐变
	-webkit-linear-gradient(颜色1,颜色2);
	径向渐变



transform:
旋转
rotate(deg);
平移
translate(x,y);
缩放
scale(x,y);
拉伸
skew(xdeg,ydeg);


css3中的运动
transition:duration style easing;
easing运动形式
transition:运动时间 样式 运动类型;


=====================================================
transition:时间 样式 运动形式;

=====================================================
阴影：
文字阴影
text-shadow:x偏移 y偏移 blur 颜色;
不只可以加一个，可以叠加
text-shadow:x y blur color,x y blur color.......;
元素阴影
box-shadow:x y blur 颜色;
可以叠加
box-shadow:[inset] x y blur [范围] 颜色;

立体按钮


ev.clientX/clientY 	鼠标在可视区中的位置
ev.clientX+scrollLeft 		ev.pageX
ev.clientY+scrollTop 		ev.pageY
ev.pageX/pageY 		鼠标在整个页面中的位置


阴影：
文字阴影
text-shadow:x y blur color;
元素阴影
box-shadow:[inset] x y blur [范围] color;


==================================================
渐变:
线性渐变
-webkit-linear-gradient(颜色,颜色2);
默认从上到下
改变渐变的方向：
-webkit-linear-gradient(方向,颜色......);
 					left top right bottom
 					0deg		默认是从左往右。

-webkit-linear-gradient(颜色 %,颜色 %....);

重复线性渐变
-webkit-repeating-linear-gradient();

径向渐变
-webkit-radial-gradient();


预习
transform:
rotate
	rotate(0deg);
	rotateX();
	rotateY();
translate
	translate(x,y);
	translateX(x);
	translateY(y);
	translateZ(z);
scale
	scale(x,y);


=======================================
今天用到的新东西要记住
















































