var gulp = require('gulp'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	concat = require('gulp-concat');
	
gulp.task('concat',function(){
	gulp.src('src/*.js')
	.pipe(concat('index.min.js'))
	.pipe(uglify())
	.pipe(gulp.dest('dest'));
});

gulp.task('default',['concat']);
