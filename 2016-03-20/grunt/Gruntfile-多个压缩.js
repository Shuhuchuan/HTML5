module.exports = function(grunt){
	//1.引入模块
	grunt.loadNpmTasks('grunt-contrib-uglify');
	//2.编写任务
	grunt.initConfig({
		uglify:{
			ya1:{
				src:'src/a.js',
				dest:'dest/a.min.js'
			},
			ya2:{
				src:'src/jquery.js',
				dest:'dest/jquery.min.js'
			}
		}
	});
	//3.注册默认任务
	grunt.registerTask('default',['uglify:ya2']);
};






