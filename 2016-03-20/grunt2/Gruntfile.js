module.exports = function(grunt){
	//1.引入模块
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-clean');
	//2.编写任务
	grunt.initConfig({
		concat:{
			con1:{
				src:'src/*.css',
				dest:'tmp/index.css'
			}
		},
		cssmin:{
			cm1:{
				src:'tmp/index.css',
				dest:'dest/index.min.css'
			}
		},
		clean:['tmp']
	});
	//3.注册默认任务
	grunt.registerTask('default',['concat','cssmin','clean']);
};






