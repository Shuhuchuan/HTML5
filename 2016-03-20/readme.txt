笔记
===============================================
git
网站

.gitignore 	有些核心的东西不想上传

git pull

开发
	git add .
	git commit -m ""

下班
git push

===================================================
自动化构建工具
grunt
官网：http://gruntjs.com/

npm node package manager

1.安装grunt命令环境
	npm install -g grunt-cli

2.真正玩grunt
需要两个文件
Gruntfile.js 	任务文件
package.json 	工程文件

编写一个最简单的任务。
运行：grunt。报错，要安装本地的grunt模块
	安装grunt模块
		npm install grunt

真正编写任务
=========================================================
一些任务，你需要找插件（模块）
模块有很多，有个人开发的，组织开发的，grunt自己开发的。
contrib开头的都是grunt自己玩的。


1.引入模块
grunt.loadNpmTasks('模块名');
2.编写任务
grunt.initConfig({
	主任务名:{
		子任务名:{
			src:'',
			dest:''
		}
	}
});
3.注册默认任务
grunt.registerTask('default',['主任务名']);

运行任务
grung
grunt 主任务名
grunt 主任务名:子任务名



各种模块
js压缩	 		grunt-contrib-uglify
css压缩 		grunt-contrib-cssmin
文件合并 		grunt-contrib-concat
删除 			grunt-contrib-clean


package.json  		工程文件
创建	
			1.手动创建
			2.自动创建
				npm init

安装模块 		
npm install 名字 --save-dev

以后只需要拷贝3个文件
src,Gruntfile.js,package.json
然后输入npm install即可


grunt 			走的是文件流 		
gulp 			走的是二进制流 		比较快，性能较高

gulp 	自动化构建工具
============================================
官网：http://gulpjs.com/

1.安装命令环境
npm install -g gulp
	
	gulp --version 		检测版本

gulpfile.js 		
package.json

安装模块
npm install 模块名字

创建gulpfile.js
引入模块
var name = require('模块名字');

gulp.task('名字',function(){
	gulp.src()
	.pipe(...)
	.pipe(gulp.dest(...));
});

gulp.task('default',['名字']);


压缩
gulp
gulp-uglify
gulp-rename
gulp-concat
del


package.json 		工程文件
不管是在grunt中还是在gulp中都一样

npm init
npm install 名字 --save-dev

拷贝gulpfile.js package.json src

npm install

======================================================
angular
================================================
angular 			MVVM

为了数据而生

面向过程
面向对象

*面向数据————————————数据就是一切。

***致力于解决交互的一些问题.(DOM,解析)

官网https://angularjs.org/
手册:http://docs.angularjs.cn/api

1.x
				1.2 	有一点差异
				1.3 	√

2.x 			完全是另一套东西了。

=============================================
关于数据 		有两个方向

数据从哪来
数据到哪去


单向绑定
ng-model->ng-bind

双向绑定
ng-model<->ng-model

绑定的另一种做法
{{}}


两个环境
原生环境
ng环境
两个环境不互通。


问题：
1.ng环境和原生环境不互通。 				√
2.初始化，写在行间不好。太乱 			√

想解决上面的问题，用controller。
===========================================
controller


获取app
var app = angular.module(app的名字,[]);
获取controller
app.controller(c的名字,function($scope){
	$scope 			ng
});

交互
app.controller(c的名字,function($scope,$http){
	//$http.get
	//$http.post
	//$http.jsonp
	$http.get(url,{params:{}}).success(fn).error(fn);
});




指令
========================================
ng-app 			angularjs管那一块
ng-model 		数据从哪来
ng-bind 		数据去哪
ng-click 		点击事件
ng-init 		初始化
ng-repeat 		迭代，循环，重复
ng-controller 	控制器
ng-show 		是否显示
ng-hide 		是否隐藏

=====================================
video 		视频
<video></video>
视频有很多格式
Chrome 		ogg 	mp4 	webB
Firefox 			mp4 	webB
IE 					mp4

controls 			控制台
autoplay 			自动播放
loop 				重复播放

oV.play() 			播放
oV.pause() 			暂停

video没有停止的方法
当前播放时间
oV.currentTime

oV.pause()
oV.currentTime = 0;
停止

oV.muted 		有没有声音

oV.volume 		调节音量
		0-1
		0没声音
		1有声音

oV.duration  		视频总时长

oV.ontimeupdate 		当时间更新

oV.webkitEnterFullScreen() 		全屏
oV.webkitExitFullScreen() 		退出全屏

================================================
audio 			音频播放
操作跟视频一模一样















































