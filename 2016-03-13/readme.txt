笔记
===================================================
WebWorker 		多线程
 	多线程，单线程


单线程 		一件事件处理完成之前，不能处理另一件事（堵着呢）
多线程 		可以同时处理多件事情

多线程： 	下载，游戏

主线程(UI) 				普通的js
工作线程(子线程) 		...

Worker 		不能跨域


斐波那契数列

Worker特点
	1.不能跨域
	2.BOM，DOM不行
	3.原理是复制
	4.子线程不能再开线程


克隆——复制
 		Worker工作原理:复制了一份

 		对象不能被克隆:
 			1.乱套了
 			2.Worker本身不允许操作dom

=================================================
WebSQL 		数据库
		Web安全?

		数据库:
		库 		不能直接存数据,管理
		表 		真正存数据的
 			行 		某一行数据
 			列 		某一项数据

 			字段 	类型
 			CHAR 		字符 			'a'
 			INT 		整数 			12
 			FLOAT 		单精度浮点数 	1.5
 			DOUBLE 		双精度浮点数 	1.355
 			VARCHAR 	字符串			"abcdfd"
 			TEXT 		文本(大字符串)

四大操作
增删改查

WebSQL
创建库
var db = openDatabase(名字,版本号,描述,大小);
创建表
CREATE TABLE user (name VARCHAR(32),password VARCHAR(32));

事物
db.transaction(function(trans){
	trans.executeSql(sql,[],function(trans,data){
		//trans 		方便下一次数据操作
		//data 			存数据
			data 		是一个resultSet
			data.rows 	是一个resultSetRowList

		成功
	},function(){
		alert('失败')
	});
});


SQL语句
插入
INSERT INTO 表名 VALUES(值1,值2....);
查询
SELECT * FROM 表名
SELECT * FROM 表名 WHERE xxx=xxx;
删除
DELETE FROM user WHERE xxxx=xxxx;

有用？没用！

注册登录
==============================================
注册
	看一下里面有没有这个用户
	如果有，就告诉人家已注册
	如果没有，就插入注册
登录
	查看有没有这个人，有就成功，没有就失败

===================================================
WebSocket——交互
 		ajax——快，省流量，实时性高

 		ajax->服务器
 		ws<->服务器

 	node	
 	官网:https://nodejs.org/en/
 		下载：
 		安装： 	无限下一步
 		检验是否安装成功： node -v

 		构建自己的服务: 		http

 	webSocket 	图

 	使用：socket.io 		简单，兼容
 		npm安装
 		npm 		node package manage


 	使用分两个方面
 		1.服务端 	引入-》连接-》发送、接收
 		2.客户端 	引入-》连接-》发送、接收


 	on 			接收
 	emit 		发送

========================================================
通信，游戏，聊天
========================================================
代码管理工具
svn 	集中式代码管理工具
	网址：https://tortoisesvn.net/
	下载-》安装
	右键，如果出现svn就对

		连接
		update
		commit

	服务器，空间，sinaapp

git 	分布式管理工具
	官网:http://msysgit.github.io/
	下载-》安装
	检测安装成功 		右键出现git

	GUI 			图形界面
	BASH 			命令行

	git --version 		检测出版本


github 			提升逼格
============================================
github.com 	注册
用邮箱注册->用户名跟你邮箱名一样
点击头像-》emails 		添加另一个邮箱		为了网站能用

git来管理github

生成SSH秘钥
ssh-keygen -t rsa -C "邮箱"
密钥在哪？
你的用户目录下有 .ssh文件夹：
	id_rsa     *id_rsa.pub

打开id_rsa.pub文件，切记：用记事本打开
把内容复制下来。
点击你的头像->settings->SSH keys

点击new ssh key
粘贴进去
点击add

配置你的电脑
git config -l 		查看你的配置信息

git config --global user.name '你的名字'
git config --global user.email '邮箱'


**注意：玩linux  只要输入命令回车不提示就绝对成功了
===================================================
本地项目上传上去

点击加号->点击New repository

git推荐项目中包含一下文件
 	README 			读我
 	LICENSE 		通行许可证 
 	.gitignore 		忽略上传

git init
git add README.txt
git commit -m "first commit"
git remote add origin https://github.com/itwenqiang/test.git
git push -u origin master


创建完文件想上传？
git status 		问他，他就告诉你该怎么做
git add . 		点代表所有
git commit -m "" 注释必须写
git push

===============================================
已有的项目下载下来
git clone 地址

git pull 			更新代码
git add .
git commit -m "注释"
..
..
git push

==============================================
创建个人站
点击加号-》点击创建
名字 		你的用户名.github.com
创建完成之后
点击settings
点击Launch automatic page generator
点击Continue to layouts
点击Publish page
网址：你的用户名.github.io



dos
切换目录		cd 路径
清屏			cls
退出 			exit
查看文件列表	dir
查看文件内容	more 文件名
linux
切换目录 		cd 路径
清屏 			clear
退出 			exit
查看文件列表 	ls
查看文件内容  	cat
编辑文件 		vi
		按i进入编辑模式
		完事之后按esc
		保存 	:wq!回车
		不保存 	:q!回车
删除文件 		rm 文件名


linux
0.安装
1.常用命令
2.yum 	安装软件
3.权限
4.配置网络
5.搭建环境
	服务器环境


linux 	ubuntu 	centOS 	
unix 	redhat

=============================================
WebWorker
多线程
	主线程
	子线程（工作线程）
	作用：
	1.主线程不卡
	2.充分利用资源
不怎么实用
	var oW = new Worker('名字.js');
	oW.postMessage(); 		发消息
	oW.onmessage=fn(ev); 		收消息
		ev.data
WebSQL
	不安全，没用
	创建一个库
	var db = openDatabase('库名','版本号','描述',容量);
	事物
	db.transaction(function(trans){
		trans.executeSql(sql,[],fn(trans,data),fn);
	});


WebSocket
	双向
	客户端<->服务端

	服务端 	引入，监听，连接，接收、发送
	var wsServer = io.listen(server);
	wsServer.on('connection',function(socket){
		......
	});
	客户端 	引入，连接，接收、发送
	io.connect('ws://地址');
	接收：socket.on('名字',function(data...){})
	发送：socket.emit('名字',data...);

git
生成秘钥，连接github
ssh-keygen -t rsa -C "邮箱"
配置
git config --global user.name ""
git config --global user.email ""
管理
git pull
git add .
git commit -m ""
git push

都把git跑通





