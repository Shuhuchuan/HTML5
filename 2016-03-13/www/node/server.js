var http = require('http'),
	fs = require('fs');

var server = http.createServer(function(req,res){
	fs.readFile('www'+req.url,function(err,data){
		if(err){
			res.write('404');
			res.end();
		}else{
			res.write(data);
			res.end();
		}
	});
});

server.listen(8080);