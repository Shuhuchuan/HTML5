var http = require('http'),
	fs = require('fs'),
	io = require('socket.io');
	
var server = http.createServer(function(req,res){
	fs.readFile('www'+req.url,function(err,data){
		if(err){
			res.end('404');
		}else{
			res.end(data);
		}
	});
});

server.listen(8080);

var wsServer = io.listen(server);

var aSock = [];
wsServer.on('connection',function(sock){
	aSock.push(sock);
	sock.on('msg',function(name,content){
		for(var i=0;i<aSock.length;i++){
			if(aSock[i]==sock)continue;
			aSock[i].emit('msg_post',name,content);
		}
	});
});












