var http = require('http'),
	fs = require('fs'),
	io = require('socket.io');

var server = http.createServer(function(req,res){
	fs.readFile('www'+req.url,function(err,data){
		if(err){
			res.write('404');
			res.end();
		}else{
			res.write(data);
			res.end();
		}
	});
});

server.listen(8080);

var wsServer = io.listen(server);

wsServer.on('connection',function(sock){
	setInterval(function(){
		sock.emit('time',new Date().getTime());
	},1000);
});







