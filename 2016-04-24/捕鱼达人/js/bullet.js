'use strict'
function Bullet(imgs,type){
	this.img = imgs['bullet'];
	
	this._size = [
		null,
		{w:24,h:26,sx:86,sy:0},
		{w:25,h:29,sx:61,sy:0},
		{w:27,h:31,sx:32,sy:35},
		{w:29,h:33,sx:30,sy:82},
		{w:30,h:34,sx:0,sy:82},
		{w:32,h:38,sx:0,sy:44},
		{w:30,h:44,sx:0,sy:0}
	];
	
	this.w = this._size[type].w;
	this.h = this._size[type].h;
	this.sx = this._size[type].sx;
	this.sy = this._size[type].sy;
	
	this.speed = 6;
}
Bullet.prototype = new Sprite();
Bullet.prototype.constructor = Bullet;