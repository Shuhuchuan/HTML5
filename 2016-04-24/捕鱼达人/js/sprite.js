'use strict'

function Sprite(img){
	this.img = img;
	this.w = 0;
	this.h = 0;
	this.sx = 0;
	this.sy = 0;
	this.x = 0;
	this.y = 0;
	this.r = 0;
	this.speed = 0;
}
Sprite.prototype.move=function(){
	//this.x+=this.speed;
	var iSpeedX = Math.sin(d2a(this.r))*this.speed;
	var iSpeedY = -Math.cos(d2a(this.r))*this.speed;
	
	this.x+=iSpeedX;
	this.y+=iSpeedY;
};
Sprite.prototype.collTest=function(obj){
	if(!obj)return;
	var x1 = this.x;
	var y1 = this.y;
	var x2 = obj.x;
	var y2 = obj.y;
	var a = x2-x1;
	var b = y2-y1;
	
	var c = Math.sqrt(a*a+b*b);
	
	if(c<30){
		return true;
	}else{
		return false;
	}
};
Sprite.prototype.draw=function(gd){
	gd.save();
	
	gd.translate(this.x,this.y);
	
	gd.rotate(d2a(this.r));
	
	gd.drawImage(
		this.img,
		this.sx,this.sy,this.w,this.h,
		-this.w/2,-this.h/2,this.w,this.h
	);
	
	gd.restore();
	
};