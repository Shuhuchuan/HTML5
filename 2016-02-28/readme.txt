笔记
===========================================================
dataset 			向后兼容自定义属性
可以遍历自定义属性

获取元素的方式
document.querySelector(); 			获取一个
document.querySelectorAll(); 		获取一组

class操作
obj.classList 		class的集合
.classList.add(); 		添加
.classList.remove(); 	删除
.classList.toggle();	切换
.classList.contains(); 	查看是否有某个class

本地存储
localStorage
相对于cookie有哪些优势
1.容量大
2.简单，方便
3.不会通过网络传输
4.（相对安全）
跟cookie有相似的地方
1.不安全
2.不能跨浏览器
3.不能跨域

================================================
css3
圆角
阴影 		box-shadow:[inset] x y blur [范围] color
文字阴影 	text-shadow:x y blur color
线性渐变 	linear-gradient([位置，角度],颜色1,颜色2....);
径向渐变	radial-gradient([位置],颜色1,颜色2......);

重复渐变
			repeating-linear-gradient([位置，角度],颜色1,颜色2....);
			repeation-radial-gradient([位置],颜色1,颜色2......);

样式前缀
-webkit- 			chrome,safari,opera
-moz- 				firefox
-ms- 				ie
-o- 				opera
**最终目标，不加前缀
不加前缀

===========================
单位
px 	em % rem deg
===========================

transform
旋转 		rotate(x,y) 		 		deg
平移 		translate(x,y) 		 		px,%,em,rem
缩放 		scale(x,y); 		 		没有单位
拉伸 		skew(x,y); 					deg

使用transform优势？
性能。因为transform只是改变人的视觉，不会改变盒子模型，不会触发重排。性能高


加事件的方式

<input type="button" onclick="" />
oBtn.onclick = fn;
oBtn.addEventListener(sEv,fn,false);

h5 很多事件都要事件绑定
****所有的咱都绑定

以后咱不用window.onload

使用DOMReady


运动
transition:duration 样式 easing [delay];

====================================================

css3选择器
属性选择器
	E[name] 		只要有这个属性名的元素
	E[name=value] 	属性的名字=属性的值
	E[name~=value] 	出现了value这个单词
	E[name*=value] 	只要出现value这个字母就行
	E[name^=value] 	以value开头
	E[name$=value] 	以value结束
	E[name|=value] 	以value-开头或只有value

结构
	E:nth-child(n) 				获取第n个
	E:nth-last-child(n) 		获取倒数第n个

	E:first-child 		E:nth-child(1)
	E:last-child 		E:nth-last-child(1)

文本选择器
E:first-line 			第一行文本
E:first-letter 			第一个文字
E::selection 			选中文字
E::after 				在后面
E::before 				在前面
================================================
background:url(),url(),url();

background-size 			设置背景图的大小
100% 100%
cover 				尽可能的放大
contain 			能显示整张图，但是不一定能撑满整个容器

==========================================================
运动
transition

transition运动结束的事件
transitionend 		需要事件绑定

动画
animation(高级运动)
@-webkit-keyframes name{
	from{}
	to{}
}
-webkit-animation:duration name timeing-function;

-webkit-animation-name:test; 					名字
-webkit-animation-duration:1s; 					时间
-webkit-animation-timing-function:ease-out; 	运动类型
-webkit-animation-iteration-count:infinite; 	迭代
							infinite 		无限
-webkit-animation-direction:alternate; 			走向
						alternate
-webkit-animation-play-state:paused; 			运动状态
					paused 			暂停
					running 		走

=====================================================
蒙版
注意：蒙版：必须是有颜色才能透出来。
使用透明的图片: 	png  gif
-webkit-mask 			
-webkit-mask-position

图片蒙版
颜色渐变蒙版


文字蒙版
background-clip:text;
注意：容器一定要先给背景色,你的文字一定要透明.
**一定要先给背景颜色，背景改了之后一定要重新设置background-clip:text;

=========================================================
注意：特别是在移动端上，尽量不要使用left，top。而是使用translate
============================================================
360°
hou 		360/12 		30
min 		360/60 		6
sec 		360/60 		6



hou 			5 		5*30
min 			20 		20*6
sec 			46 		46*6

改变原点
transform-origin:x y;
				center left top bottom right
				px	%


一个刻度多少度
6°

0
1
2
3
4
5 			1
6
7
8
9
10 			2
15 			3
20 			4
25 			5


min 		30/60 			hou +15
sec 		30/60 			min +3
=====================================================
transform
正数，都是顺时针，负数，逆时针
rotate(35deg) 		
rotateX(35deg) 		 	你站在右边看它	
rotateY(35deg) 			你站在下边看它


透视
transform:perspective(px);
			数值越小，效果越明显，数值越大，效果越不明显

注意：如果没有父级，就给这个元素加。如果有父级，就给父级加。

========================================
注意：transform不能改行元素.块元素，行内块元素都行

注意：transform。先写的后执行，后写的先执行.

['l2','l1','cur','r1','r2','','','','','','']
['','l2','l1','cur','r1','r2','','','','','']
['','','l2','l1','cur','r1','r2','','','','']
['','','','l2','l1','cur','r1','r2','','','']

=========================================
translate(x,y)
translateX()
translateY()
translateZ()


把2d变成3d
transform-style:preserve-3d;
加给父级。


作业：例子。

作业发我邮箱：
标题：姓名+日期
itwenqiang@sina.com